<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pokédex</title>
    </head>
    <body>
		<center>
		<form action="search.php" method="get">
		<label for="search">Enter an ID for the Pokedex</label>
		<input type="text" name="search" id="search">
		<input type="submit" name="submit" value="Search">
		</form>
		</center>

	<?php
	// If the search box isn't filled
	if(!isset($_GET['search']))
	{
		echo "Please enter a Search Term";
		exit;
	}
	
	// Retrieve entry from the search box
	$id = $_GET['search'];
	$base = "http://pokeapi.co/api/v2/pokemon/"; // Access Pokeapi
	
	// If the entry in the search box isn't a number
	if (!is_numeric($id)){
		echo "Please enter a valid Search Term (An ID Number up to 722)";
	} else{
		$data = file_get_contents($base.$id.'/'); // Access file contents from api for each ID
		echo json_encode($data); // Print out JSON Data of the search term
	}
	?>	
    </body>
</html>