<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pok�dex</title>
    </head>
    <body>
	<?php
	// Access Pokeapi
	$base = "http://pokeapi.co/api/v2/pokemon/";
	?>
	"<table border = 1>
	 <tr>
	  <th>Picture</th>
	  <th>Name</th>
	  <th>Species</th>
	  <th>Height</th>
	  <th>Weight</th>
	  <th>Abilities</th>
	 </tr>";
	 <?php
	 // For every id in the collection less than 10
	for($id = 1; $id < 10; $id++){
		// Access file contents from api for each ID
		$data = file_get_contents($base.$id.'/');
		$pokemon = json_decode($data); // Decode the JSON Data
		// echo a table with the api data in
		echo '<tr>
			  <td> <img src="'. $pokemon->sprites->front_default .'" width="30px" />
		</td>
		<td>
			'. $pokemon->name .'
		</td>
		<td>
			'. $pokemon->species->name.'
		</td>
		<td>
			'. $pokemon->height .'
		</td>
		<td>
			'. $pokemon->weight .'
		</td>
		<td>
			'. print json_encode($pokemon->abilities[0]) .'
		</td>
		</tr>';
	}
	echo '</table>';
	?>
    </body>
</html>
