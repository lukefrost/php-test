<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pokédex</title>
    </head>
    <body>
	<?php
	$base = "http://pokeapi.co/api/v2/pokemon/"; // Access Pokeapi
	$id = 1; // Define ID for the Pokemon you want to access from the pokeapi
	$data = file_get_contents($base.$id.'/'); // Access file contents from api for each ID
	$pokemon = json_decode($data); // Decode the JSON Data
	// Print out details from the API
	echo $pokemon->sprites->front_default."<br>";
	echo $pokemon->name."<br>";
	echo $pokemon->species->name."<br>";
	echo $pokemon->height."<br>";
	echo $pokemon->weight."<br>";
	print json_encode($pokemon->abilities[0]);"<br>";
	?>
    </body>
</html>